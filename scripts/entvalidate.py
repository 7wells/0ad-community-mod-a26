#!/usr/bin/env python3
import argparse
from os import chdir
from pathlib import Path
from subprocess import run, CalledProcessError
from sys import exit
from xml.etree import ElementTree
from .scriptlib import SimulTemplateEntity, find_files
import logging

logger = logging.getLogger(__name__)

root = Path()
relaxng_schema = root / "scripts" / "entity.rng"
vfs_root = root
mod = "community-mod"

def main():
    if not relaxng_schema.exists():
        print(f"""Relax NG schema non existant.
Please create the file {relaxng_schema.relative_to(root)}
You can do that by running 'pyrogenesis -dumpSchema' in the 'system' directory""")
        exit(1)
    if run(['xmllint', '--version'], capture_output=True).returncode != 0:
        print("xmllint not found in your PATH, please install it (usually in libxml2 package)")
        exit(2)

    parser = argparse.ArgumentParser(description='Validate templates')
    parser.add_argument('-p','--path', nargs='*', help='Optionally, a list of templates to validate.')
    args = parser.parse_args()

    simul_templates_path = Path('simulation/templates')
    simul_template_entity = SimulTemplateEntity(vfs_root, logger)
    count = 0
    failed = 0
    templates = sorted([(Path(p), None) for p in args.path]) if args.path else sorted(find_files(vfs_root, [mod], 'simulation/templates', 'xml'))
    for fp, _ in templates:
        if fp.stem.startswith('template_'):
            continue
        path = fp.as_posix()
        if path.startswith('simulation/templates/mixins/') or path.startswith("simulation/templates/special/"):
            continue
        print(f"# {fp}...")
        count += 1
        entity = simul_template_entity.load_inherited(simul_templates_path, str(fp.relative_to(simul_templates_path)), [mod])
        xmlcontent = ElementTree.tostring(entity, encoding='unicode')
        try:
            run(['xmllint', '--relaxng', str(relaxng_schema.resolve()), '-'], input=xmlcontent, capture_output=True, text=True, check=True)
        except CalledProcessError as e:
            failed += 1
            print(e.stderr)
            print(e.stdout)
    print(f"\nTotal: {count}; failed: {failed}")


if __name__ == '__main__':
    main()
