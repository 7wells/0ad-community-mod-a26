import json
from . import MOD_PATH, check_cwd_is_correct

def get_version():
	with open(MOD_PATH / "mod.json", "r") as f:
		mod = json.load(f)
		print(mod['version'])

if __name__ == '__main__':
	check_cwd_is_correct()
	get_version()
else:
	raise Exception("Must be called directly")
